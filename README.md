# Precipitation
A DLL that makes precipitation in Blockland collide with bricks that have raycasting on. This affects your client only.

## Usage
Inject the DLL into Blockland via [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader) or your favorite DLL injector.
