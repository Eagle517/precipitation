#include <windows.h>

#include "TSHooks.hpp"

ADDR gPrecipitation__findDropCutoffTypemaskLoc;

BlFunctionDef(void, __thiscall, Precipitation__interpolateTick, ADDR, float);
BlFunctionHookDef(Precipitation__interpolateTick);

/* Precipitation checks for collision at its center. The client also
   interpolates a drop's render position a bit too far. To avoid drops
   clipping through thin bricks, I make it so drops will clamp their z
   rendering position to where they are supposed to collide and force
   the drop to hit on the next ::processTick call. This works well enough. */
void __fastcall Precipitation__interpolateTickHook(ADDR obj, void *blank, float delta)
{
	Precipitation__interpolateTickOriginal(obj, delta);

	float mDropSize = *(float *)(obj + 524);

	float *dropRenderZ;
	float *dropPosZ;
	float *dropHitZ;

	ADDR mDropHead = *(ADDR *)(obj + 540);
	while(mDropHead)
	{
		dropRenderZ = (float *)(mDropHead + 24);
		dropPosZ = (float *)(mDropHead + 12);
		dropHitZ = (float *)(mDropHead + 52);

		if(*(BYTE *)(mDropHead + 41) && *dropRenderZ - mDropSize <= *dropHitZ)
		{
			*dropRenderZ = *dropHitZ + mDropSize;
			*dropPosZ = *dropHitZ - 0.25;
		}

		mDropHead = *(ADDR *)(mDropHead + 76);
	}
}

bool init()
{
	BlInit;

	BlScanFunctionHex(Precipitation__interpolateTick, "55 8B EC 83 E4 F8 A1 ? ? ? ? 83 EC 50");
	BlScanHex(gPrecipitation__findDropCutoffTypemaskLoc, "68 ? ? ? ? 8D 44 24 30 50 D9 5C 24 24");

	//Change the typemask used for precipitation collision from 25174016 to 58728448 (25174016 | $TypeMasks::FxBrickObjectType)
	BlPatchByte(gPrecipitation__findDropCutoffTypemaskLoc + 4, 0x03);

	BlCreateHook(Precipitation__interpolateTick);
	BlTestEnableHook(Precipitation__interpolateTick);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit()
{
	BlPatchByte(gPrecipitation__findDropCutoffTypemaskLoc + 4, 0x01);
	BlTestDisableHook(Precipitation__interpolateTick);

	BlPrintf("%s: deinit'd", PROJECT_NAME);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
